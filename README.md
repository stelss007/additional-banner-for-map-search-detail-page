#Additional banner on mapsearch detail page

For using additional banner on mapsearch detail page you need copy this folder *'map-search-additional-banner'*
to **active_theme_directory/assets**. Then you need add code code below to functions.php:  

```php
function add_map_search_customization() {
    echo '<script type="text/javascript">var obj = { theme: "' . get_stylesheet_directory_uri() . '"};  window.hjPath = obj;</script>';
    echo '<script type="text/javascript" src="' . get_stylesheet_directory_uri() . '/assets/map-search-additional-banner/js/map-search-additional-banner.js"></script>';
}
add_action('hji_map_search_footer', 'add_map_search_customization');
```

You can edit map-search-additional-banner.css
and if you want change banner template you can edit code on the additionalBannerTemplate() method of map-search-additional-banner.js file
