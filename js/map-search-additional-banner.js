'use strict';

class mapSearchAdditionalBanner
{
  constructor(hj) {
    self = this;
    self.hj = hj;
    self.events();
  }

  /**
   * @link https://paul.kinlan.me/waiting-for-an-element-to-be-created/
   * @param selector
   * @returns {Promise<any>}
   */
  waitForElement(selector) {
    return new Promise(function(resolve, reject) {
      var element = document.querySelector(selector);

      if(element) {
        resolve(element);
        return;
      }

      var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
          var nodes = Array.from(mutation.addedNodes);

          for(var node of nodes) {
            if(node.matches && node.matches(selector)) {
              observer.disconnect();
              resolve(node);
              return;
            }
          };
        });
      });

      observer.observe(document.documentElement, {
        childList: true,
        subtree: true
      });

    });
  }

  /**
   * Sets up our events...duh
   */
  events() {
    self.hj.on('listing-view.updated', function(el, listing, obj, Vue) {
        if(window.Vue === undefined) {
          window.Vue = Vue;
        }
    });

    self.hj.on('listing.viewed', function(listing) {
      self.addSingleListingContent(listing);
    });
  }

  /**
   * Adds the initial content to the single listing view
   *
   * @param listing
   */
  addSingleListingContent(listing)
  {
    self.waitForElement('#hj-app').then(function (element) {
      var detailsContent = document.querySelector('.details-content');
      var infoBlock = document.querySelector('.info');
      self.addRibbon (detailsContent, infoBlock.nextSibling, listing);

    });
  }

  addRibbon(container, beforElement, listing) {
    var ribbonBanner = document.querySelector('.ribbon-container');

    if (null === ribbonBanner) {
      ribbonBanner = document.createElement('div');
      ribbonBanner.className = 'ribbon-container';

      var head = document.querySelector('head');
      var linkElement = document.createElement('link');
      linkElement.setAttribute('rel', 'stylesheet');
      linkElement.setAttribute('type', 'text/css');
      //linkElement.setAttribute('href', '/wp-content/themes/hji-brian-russo/assets/css/ribbon.css?ver=1.0');
      linkElement.setAttribute('href', obj.theme + '/assets/map-search-additional-banner/css/map-search-additional-banner.css?ver=1.0');

      head.appendChild(linkElement);
    }

    ribbonBanner.innerHTML = self.additionalBannerTemplate(listing);

    container.insertBefore(ribbonBanner, beforElement);
  }


  formatMoney(number, decPlaces, decSep, thouSep) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
      decSep = typeof decSep === "undefined" ? "." : decSep;
    thouSep = typeof thouSep === "undefined" ? "," : thouSep;

    var sign = number < 0 ? "-" : "";
    var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var j = (j = i.length) > 3 ? j % 3 : 0;

    return sign +
      (j ? i.substr(0, j) + thouSep : "") +
      i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
      (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
  }

  additionalBannerTemplate(listing) {
    let ribbonPrice = (listing.listPrice * 0.03) - 4995;
    let ribbonTempHeight = (window.innerWidth >= 1024) ? 70 : 103;

    if (0 > ribbonPrice) {
      ribbonPrice = 0;
    }

    return `
        <h1 class="ribbon" style="--ribbon_height: ${ribbonTempHeight}px">
          <div class="ribbon-content">
            <div class="checkcss"></div>
            <div class="ribbon-content__line-1">
                <strong>
                  <span class="line-1--part1">Buy with
                      <img class="ribbon-logo" src="${obj.theme }/assets/img/logo.png" alt=""> and
                  </span>
                    <span class="line-1--part2">
                      receive an estimated rebate of
                      <span style="color: #28acf0; font-size: 23px;">$${self.formatMoney(ribbonPrice, 2, '.' , ',')}</span>
                      on this home.
                    </span>
                  </strong>
            </div>
            <div class="ribbon-content__line-2">
                Home Change will rebate any selling agent commission over $4,995 offered by the homeowner.
            </div>
          </div>
        </h1>
    `;
  }
}

// hooks into map search callback
function smCustom(hj)
{
  const customization = new mapSearchAdditionalBanner(hj);
}
